#ifndef __easyio_drv_h__
#define __easyio_drv_h__

#define LED_A 100
#define LED_B 101
#define LED_LIGHT
#define LED_LAMP

extern void ActiveGprsModemImpl(void);
extern void ResetGprsModemImpl(void);
extern void LedCtlImpl(int index , char status);
extern void EnableWGImpl(void);
extern void SerialPortSettingImpl(char *atCmdPort , char *trans1Port[] , char *trans2Port[]);


#endif
